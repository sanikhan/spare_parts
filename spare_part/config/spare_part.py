from __future__ import unicode_literals
from frappe import _, get_list

def get_data():
	return [
		{
			"label": _("Documents"),
			"icon": "fa fa-cog",
			"items": [
				{
					"type": "doctype",
					"name": "Query"
				},
				{
					"type": "doctype",
					"name": "Review Query"
				},{
					"type": "doctype",
					"name": "Quotation"
				},{
					"type": "doctype",
					"name": "Sales Order"
				},{
					"type": "doctype",
					"name": "Sales Invoice"
				},{
					"type": "doctype",
					"name": "Delivery Note",
					"label": _("Chalan")
				}
			]
		},
		{
			"label": _("Setup"),
			"icon": "fa fa-conf",
			"items": [
				{
					"type": "doctype",
					"name": "Engine Model",
				},{
					"type": "doctype",
					"name": "Alt Model",
				},{
					"type": "doctype",
					"name": "TC Model",
				}
			]
		},
		{
			"label": _("Reports"),
			"icon": "fa fa-list",
			"items": [
				{
					"type": "report",
					"name": "Taken Under Loan",
					"label": _("Purchase On Loan"),
					"doctype": "Stock Entry",
					"is_query_report": True
				},
				{
					"type": "report",
					"name": "Given Under Loan",
					"label": _("Given Under Loan"),
					"doctype": "Stock Entry",
					"is_query_report": True
				},
				{
					"type": "report",
					"name": "Delivered Items To Be Billed",
					"label": _("Pending Invoice"),
					"doctype": "Sales Invoice",
					"is_query_report": True
				},
				{
					"type": "report",
					"name": "Awaiting Collection from CESAP",
					"label": _("Awaiting Collection from CESAP"),
					"doctype": "Stock Entry",
					"is_query_report": True
				}
			]
		}
	]
