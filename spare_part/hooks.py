# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from . import __version__ as app_version
# from frappe.utils import money_in_words

app_name = "spare_part"
app_title = "Spare Part"
app_publisher = "Mainul Islam"
app_description = "ERPNext for Spare Part"
app_icon = "octicon octicon-tools"
app_color = "green"
app_email = "mainulkhan94@gmail.com"
app_license = "MIT"

# Includes in <head>
# ------------------

boot_session = 'spare_part.boot.get_bootinfo'

# include js, css files in header of desk.html
app_include_css = "assets/spare_part/css/spare_part.css"
app_include_js = "assets/js/spare_part.js"

# include js, css files in header of web template
# web_include_css = "/assets/spare_part/css/spare_part.css"
# web_include_js = "/assets/spare_part/js/spare_part.js"

# include js in page
page_js = {"desktop" : "public/js/dashboard.js"}

# include js in doctype views
doctype_js = {
    "Quotation": "public/js/quotation.js",
    "Sales Order": "public/js/sales_order.js",
    "Sales Invoice": "public/js/sales_invoice.js",
    "Item": "public/js/item.js",
    "Stock Entry": "public/js/stock_entry.js"
    }
doctype_list_js = {
    "Quotation": "public/js/quotation_list.js",
    "Sales Invoice": "public/js/sales_invoice_list.js",
    "Purchase Receipt": "public/js/purchase_receipt_list.js"
    }
# doctype_tree_js = {"doctype" : "public/js/doctype_tree.js"}
# doctype_calendar_js = {"doctype" : "public/js/doctype_calendar.js"}

# Home Pages
# ----------

# application home page (will override Website Settings)
# home_page = "login"

# website user home page (by Role)
# role_home_page = {
#	"Role": "home_page"
# }

# Website user home page (by function)
# get_website_user_home_page = "spare_part.utils.get_home_page"

# Generators
# ----------

# automatically create page for each record of this doctype
# website_generators = ["Web Page"]

# Installation
# ------------

# before_install = "spare_part.install.before_install"
# after_install = "spare_part.install.after_install"

# Desk Notifications
# ------------------
# See frappe.core.notifications.get_notification_config

# notification_config = "spare_part.notifications.get_notification_config"

# Permissions
# -----------
# Permissions evaluated in scripted ways

# permission_query_conditions = {
# 	"Event": "frappe.desk.doctype.event.event.get_permission_query_conditions",
# }
#
# has_permission = {
# 	"Event": "frappe.desk.doctype.event.event.has_permission",
# }

# Document Events
# ---------------
# Hook on document methods and events

# doc_events = {
# 	"Item": {
# 		"after_save": "spare_part.utils.events.get_data"
# 	}
# }


# Scheduled Tasks
# ---------------

# scheduler_events = {
# 	"all": [
# 		"spare_part.tasks.all"
# 	],
# 	"daily": [
# 		"spare_part.tasks.daily"
# 	],
# 	"hourly": [
# 		"spare_part.tasks.hourly"
# 	],
# 	"weekly": [
# 		"spare_part.tasks.weekly"
# 	]
# 	"monthly": [
# 		"spare_part.tasks.monthly"
# 	]
# }

# Testing
# -------

# before_tests = "spare_part.install.before_tests"

# Overriding Whitelisted Methods
# ------------------------------
#
# override_whitelisted_methods = {
# 	"erpnext.selling.doctype.sales_order.sales_order.make_sales_invoice": "spare_part.utils.sales.make_invoice_from_so"
# }

website_context = {
	"favicon": 	"/assets/spare_part/images/favicon.ico",
	"splash_image": "/assets/spare_part/images/kebd-logo.jpg"
}

# JENV
jenv = {
    "methods": ["in_words:frappe.utils.money_in_words"]
}

on_session_creation = [
	"spare_part.utils.redirect_to_user_dashboard"
]