from __future__ import unicode_literals
import frappe
from erpnext.selling.doctype.sales_order.sales_order import make_sales_invoice

import frappe.defaults
from frappe.utils import cint, flt, add_months, today, date_diff, getdate, add_days, cstr, nowdate
from frappe import _, msgprint, throw, scrub, ValidationError
from frappe.model.mapper import get_mapped_doc


@frappe.whitelist()
def make_invoice_from_so(source_name, target_doc=None, ignore_permissions=False):
    doclist = make_sales_invoice(source_name, target_doc, ignore_permissions)
    quotation = None
    for item in doclist.items:
        if not quotation and item.so_detail:
            quotation = frappe.db.get_value("Sales Order Item", item.so_detail, "prevdoc_docname")
        if quotation:
            break
    if not quotation:
        return doclist
    quotation_doc = frappe.get_doc("Quotation", quotation)	
    if quotation_doc._quotation_for == "Local":
        if quotation_doc.discount_amount:		# this section added by sani to adjust the calculation and avoid discount  for local invoice
            rate = quotation_doc.total_sales / quotation_doc.total
            items = doclist.as_dict().get('items', [])
            for item in items:
                item.rate = item.base_net_rate
                item.price_list_rate = 0

            doclist.set('items', items)
            doclist.discount_amount = 0.0
            doclist.run_method("calculate_taxes_and_totals")
            return doclist
        else:
            return doclist

    elif quotation_doc._quotation_for == "Foreign":   
        rate = quotation_doc.total_sales / quotation_doc.total
        items = doclist.as_dict().get('items', [])
        for item in items:
            p_diff = item.rate - item.price_list_rate
            item.rate = item.rate * rate
            item.price_list_rate = item.rate - p_diff

        doclist.set('items', items)
        doclist.discount_amount = 0.0
        doclist.run_method("calculate_taxes_and_totals")
        return doclist


@frappe.whitelist()
def make_delivery_note(source_name, target_doc=None):
    def set_missing_values(source, target):
        target.ignore_pricing_rule = 1
        target.run_method("set_missing_values")
        target.run_method("calculate_taxes_and_totals")

    def update_item(source_doc, target_doc, source_parent):
        target_doc.qty = flt(source_doc.qty) - flt(source_doc.delivered_qty)
        target_doc.stock_qty = target_doc.qty * flt(source_doc.conversion_factor)

        target_doc.base_amount = target_doc.qty * flt(source_doc.base_rate)
        target_doc.amount = target_doc.qty * flt(source_doc.rate)

    doclist = get_mapped_doc("Sales Invoice", source_name, 	{
        "Sales Invoice": {
            "doctype": "Delivery Note",
            "validation": {
                "docstatus": ["=", 1]
            }
        },
        "Sales Invoice Item": {
            "doctype": "Delivery Note Item",
            "field_map": {
                "name": "si_detail",
                "parent": "against_sales_invoice",
                "serial_no": "serial_no",
                "sales_order": "against_sales_order",
                "so_detail": "so_detail",
                "cost_center": "cost_center"
            },
            "postprocess": update_item,
            "condition": lambda doc: doc.delivered_by_supplier!=1
        },
        "Sales Taxes and Charges": {
            "doctype": "Sales Taxes and Charges",
            "add_if_empty": True
        },
        "Sales Team": {
            "doctype": "Sales Team",
            "field_map": {
                "incentives": "incentives"
            },
            "add_if_empty": True
        }
    }, target_doc, set_missing_values)

    return doclist