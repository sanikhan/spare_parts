import frappe

@frappe.whitelist()
def redirect_to_user_dashboard():
	if frappe.local.response["home_page"]=="/desk" and 'kebd admin' in frappe.get_roles():
		frappe.local.response["home_page"] = '/desk#desktop'