import frappe
# from frappe.desk.reportview import get_match_cond, get_filters_cond
from frappe.model.mapper import get_mapped_doc
from frappe.utils import flt
from erpnext.stock.get_item_details import get_conversion_factor


@frappe.whitelist()
def make_review_query(source_name, target_doc=None, ignore_permissions=False):
	doclist = get_mapped_doc("Query", source_name, {
			"Query": {
				"doctype": "Review Query",
				"validation": {
					"docstatus": ["=", 1]
				},
				"field_map": [
					['name', 'query']
				]
			},
			"Query Engine": {
				"doctype": "Query Engine",
				"add_if_empty": True
			},
			"Opportunity Item": {
				"doctype": "Review Query Item"
			}
		}, target_doc, ignore_permissions=ignore_permissions)

	return doclist

def update_item_values(target_doc):
	"""Target Doc is an Child Doc"""
	if target_doc.item_code:
		item_dict = frappe.get_doc("Item", target_doc.item_code)
		fields = {'item_name': 'item_name', 'description': 'description', 'stock_uom': 'default_uom',
		 'country_of_origin': 'country_of_origin', 'hs_code': 'hs_code', 'weight_per_unit': 'weight_per_unit', 'weight_uom': 'weight_uom'}
		for k, v in fields.items():
			if not target_doc.get(k) and item_dict.get(v):
				target_doc.set(k, item_dict.get(v))


@frappe.whitelist()
def make_quotation(source_name, target_doc=None, ignore_permissions=False):
	def update_total_weight(source_doc, target_doc):
		total_weight = sum([x.total_weight for x in target_doc.items or [] if x.total_weight])
		target_doc.total_net_weight = total_weight

	def update_item(source_doc, target_doc, source_parent):
		# target_doc.payment_amount = -source_doc.payment_amount
		update_item_values(target_doc)
		if not target_doc.conversion_factor:
			target_doc.conversion_factor = 1.0 if target_doc.stock_uom == target_doc.uom else get_conversion_factor(target_doc.item_code, target_doc.uom)
			target_doc.stock_qty = flt(target_doc.qty) * target_doc.conversion_factor
		if target_doc.weight_per_unit:
			total = flt(target_doc.weight_per_unit)* (flt(target_doc.stock_qty) or 1.0)
			target_doc.total_weight = total

	doclist = get_mapped_doc("Review Query", source_name, {
			"Review Query": {
				"doctype": "Quotation",
				"validation": {
					"docstatus": ["=", 1]
				},
				"field_map": [
					['name', 'review_query']
				]
			},
			"Query Engine": {
				"doctype": "Query Engine",
				"add_if_empty": True
			},
			"Review Query Item": {
				"doctype": "Quotation Item",
				"postprocess": update_item
			}
		}, target_doc, update_total_weight, ignore_permissions=ignore_permissions)

	return doclist


@frappe.whitelist()
def get_incoming_rate():
	dt = "incoming_rate"
	tbname = "batch_number"
	batch_num = "batch_no"
	batch_value = "45A9C0E"
	wh = "warehouse"
	wh_value = "KEBD Main stock - SP"
	in_rate = frappe.db.sql(""" select %s from `tab%s` where %s = %s and %s = %s """, dt,tbname,batch_num,batch_value,wh,wh_value)
	print("Incoming Rate",in_rate)



@frappe.whitelist()    #To get count of sales Invoice where status is overdue for specific customer
def get_invoice_count(cus_name):
	print("Customer Name: ",cus_name)
	res =  frappe.db.sql("""select count(*)
				from `tabSales Invoice` where status = "Overdue" and customer_name = '{}' """ .format(cus_name))
	print("Res",res[0][0])
	return res[0][0]

@frappe.whitelist()
def get_reference_rate(item_code, customer):
	return frappe.db.sql("""select rate from `tabSales Order Item` left join `tabSales Order` on `tabSales Order Item`.parent = `tabSales Order`.name
	where `tabSales Order`.docstatus = 1 and customer = '{}' and item_code = '{}' """.format(customer, item_code))


@frappe.whitelist()
def get_status(doctype, status_field='status'):
	d = frappe.db.sql("select count({0}), {0} from `tab{1}` where docstatus < 2 group by {0}".format(status_field, doctype))
	labels, values = [], []
	for x in d:
		labels.append(x[1])
		values.append(x[0])
	return {
		"type": "percentage",
		"data": {
			'labels': labels,
			'datasets': [
				{"name": doctype, "values":values}
			]
		}
	}