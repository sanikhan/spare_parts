from __future__ import unicode_literals
import frappe
from erpnext.selling.doctype.quotation.quotation import Quotation
from erpnext.selling.doctype.sales_order.sales_order import SalesOrder


def update_status(self, update=False, status=None, update_modified=True):
    super(Quotation, self).set_status(update, status, update_modified)
    if self.review_query:
        re_q = frappe.get_doc("Review Query", self.review_query)
        re_q.flags.ignore_validate_update_after_submit = True
        re_q.run_method('validate')
        re_q.save()


def sales_update_status(self, update=False, status=None, update_modified=True):
    super(SalesOrder, self).set_status(update, status, update_modified)
    for quotation in list(set([d.prevdoc_docname for d in self.get("items")])):
        if quotation:
            re_q = frappe.get_doc("Quotation", quotation)
            re_q.run_method('validate')
            re_q.flags.ignore_validate_update_after_submit = True
            re_q.save()


def sales_before_update_after_submit(self):
    # super(SalesOrder, self).before_update_after_submit()
    self.set_status()