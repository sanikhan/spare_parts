import frappe
from .utils.events import update_status, sales_update_status, sales_before_update_after_submit
from erpnext.selling.doctype.quotation.quotation import Quotation
from erpnext.selling.doctype.sales_order.sales_order import SalesOrder

def get_bootinfo(bootinfo):
	cache = frappe.cache()
	cache.set_value('update-info', None)
	Quotation.set_status = update_status
	SalesOrder.set_status = sales_update_status
	SalesOrder.before_update_after_submit = sales_before_update_after_submit