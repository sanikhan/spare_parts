# -*- coding: utf-8 -*-
# Copyright (c) 2018, Mainul Islam and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from types import MethodType
from frappe.model.document import Document
from spare_part.spare_part.doctype.query.query import Query

class ReviewQuery(Document):
	update_status = Query.update_status.__func__
	_get_status = Query._get_status.__func__
	
	def validate(self):
		self.update_status()

	def on_update(self):
		self.update_prevdoc()

	def update_prevdoc(self, status=None):
		if self.query:
			doc = frappe.get_doc("Query", self.query)
			doc.update_status(status)
			doc.save()