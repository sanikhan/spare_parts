// Copyright (c) 2018, Mainul Islam and contributors
// For license information, please see license.txt

frappe.provide('spare_part.query')

{% include "spare_part/public/js/query.js" %}

spare_part.query.ReviewQueryController = spare_part.query.CommonController.extend({
	refresh: function() {
		let me = this;
		if (me.frm.doc.docstatus===0){
			me.frm.add_custom_button(__("Query"), ()=> me.get_from_query(), __("Get From"))
		}else if (me.frm.doc.docstatus===1) {
			me.frm.add_custom_button(__("Quotation"), ()=> me.make_quotation(), __("Make"))
			me.frm.page.set_inner_btn_group_as_primary(__("Make"))
		}

        if (this.frm.doc.customer && !this.frm.called_customer_method){
            this.frm.script_manager.trigger('customer')
            this.frm.called_customer_method = true
        }
	},
	get_from_query() {
		let me = this;
		if (!me.frm.query_dialog) {
			me.frm.query_dialog = new frappe.ui.Dialog({
				title: __("Get From Query"),
				fields: [
					{
						fieldname: 'query',
						fieldtype: 'Link',
						options: 'Query',
						reqd: 1,
						label: __("Query")
					}
				]
			})
			me.frm.query_dialog.set_primary_action(__("Get"), function(values){
				if (!values.query)
					return;
				me.frm.called_customer_method = true
				erpnext.utils.map_current_doc({
					method: "spare_part.utils.queries.make_review_query",
					doctype: 'Query',
					source_name: values.query
				})
				me.frm.query_dialog.hide()
			})
		}
		me.frm.query_dialog.show()	
	},
	make_quotation: function() {
		frappe.model.open_mapped_doc({
			method: "spare_part.utils.queries.make_quotation",
			frm: me.frm
		})
	}
})

cur_frm.cscript = new spare_part.query.ReviewQueryController({frm: cur_frm})