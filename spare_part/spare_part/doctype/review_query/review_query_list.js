
frappe.listview_settings['Review Query'] = {
	get_indicator: function(doc) {
        return [__(doc.status), "", "status,=,"+doc.status]
    },
    onload(list) {
        new window.make_status_char_for_list(list)
    }
};
