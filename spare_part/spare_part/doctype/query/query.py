# -*- coding: utf-8 -*-
# Copyright (c) 2018, Mainul Islam and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
from spare_part.utils.queries import update_item_values

class Query(Document):
	def validate(self):
		for item in self.items or []:
			update_item_values(item)
		self.update_status()

	def update_status(self, status=None):
		if not status: 
			pass
		status = self._get_status()
		if self.status != status:
			self.db_set('status', status, False)
		if self.doctype == "Review Query":
			self.update_prevdoc(status)

	def _get_status(self):
		status = self.status or "Query"
		related_docs = {}
		if self.doctype == "Query" and frappe.db.exists("Review Query", {"query": self.name, "docstatus": ("<", 2)}):
			status = "Review Query"
			related_docs['review_query'] = frappe.get_value("Review Query", {"query": self.name, "docstatus": ("<", 2)})
		elif self.doctype == "Review Query":
			status = "Review Query"
			related_docs['review_query'] = self.name
		if not related_docs.get('review_query'):
			return status
		if frappe.db.exists("Quotation", {"review_query": related_docs['review_query'], "docstatus": ('<', 2)}):
			related_docs['quotation'] = frappe.get_value("Quotation", {"review_query": related_docs['review_query'], "docstatus": ('<', 2)})
			status = "Quotation"
		if not related_docs.get('quotation'):
			return status
		quotation = frappe.get_doc("Quotation", related_docs['quotation'])
		if quotation.status == "Ordered":
			so = frappe.db.get_value("Sales Order Item", {"prevdoc_docname": quotation.name, "docstatus": 1}, 'parent')
			status = frappe.get_value("Sales Order", so, 'status')
		elif quotation.status == "Lost":
			status = "Lost"
		return status
