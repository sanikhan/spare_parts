// Copyright (c) 2018, Mainul Islam and contributors
// For license information, please see license.txt

{% include "spare_part/public/js/query.js" %}

spare_part.query.QueryController = spare_part.query.CommonController.extend({
	refresh() {
		// this._super()
		let me = this;
		if (!me.frm.is_new() && me.frm.doc.docstatus===1){
			this.frm.add_custom_button(__("Review Query"), ()=> me.make_review_query(), __("Make"))
		}
	},
	make_review_query() {
		let me = this;
		frappe.model.open_mapped_doc({
			method: "spare_part.utils.queries.make_review_query",
			frm: me.frm
		})
	}
})

cur_frm.cscript = new spare_part.query.QueryController({frm: cur_frm})