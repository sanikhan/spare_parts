# Copyright (c) 2013, Mainul Islam and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe import _


def execute(filters=None):
	return get_columns(), get_data(filters)

def get_columns():
	return [
		_("Customer") + ":Link/Customer:180",
		_("PO Ref No") + ":Link/Item:180",
		_("DATE - Readiness") + ":Date:100",
		_("DATE -  Collection") + ":Date:100",
		_("Aging Days") + ":Int:120",
		_("Remarks") + ":Data:200"
	]

def get_data(filters):
	existigs = frappe.db.sql("select load_entry from `tabStock Entry` where purpose = 'Material Receipt' and docstatus=1 and load_entry is not null") or []
	return frappe.db.sql("""select customer, item_code, delivery_date, return_date, DATEDIFF(NOW(), delivery_date), remarks from `tabStock Entry` 
	left join `tabStock Entry Detail` on `tabStock Entry`.name = `tabStock Entry Detail`.parent
	where `tabStock Entry`.name not in ('{}') and `tabStock Entry`.docstatus=1 and purpose ='Material Issue' and customer is not null""".format(','.join(set([x[0] for x in existigs]))))
