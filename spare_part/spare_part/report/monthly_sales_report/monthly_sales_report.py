# Copyright (c) 2013, Mainul Islam and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe import _

def execute(filters=None):
	return get_columns(), get_data(filters)


def get_columns():
	return [
		_("Date") + ":Date:100",
		_("Customer") + ":Link/Customer:150",
		_("KAM") + ":Link/Sales Person:100",
		_("Invoice No") + ":Link/Sales Invoice:100",
		_("Query No") + ":Link/Query:100",
		_("Parts Name") + ":Data:150",
		_("Part No") + ":Link/Item:150",
		_("Unit Price") + ":Currency:100",
		_("Material Price") + ":Currency:100",
		_("Total Price") + ":Currency:100"
	]


def get_data(filters=None):
	conditions = "si.docstatus=1"
	if filters.get('from_date'):
		conditions += " and date(sii.posting_date) >= date('{}')".format(filters.get('from_date'))
	if filters.get('to_date'):
		conditions += " and date(sii.posting_date) <= date('{}')".format(filters.get('to_date'))
	return frappe.db.sql("""select si.posting_date, si.customer, si.sales_person, si.name, revq.query, sii.item_name, sii.item_code,
	sii.qty, quoi.valuation_rate, sii.amount from `tabSales Invoice` as si
	left join `tabSales Invoice Item` as sii on si.name = sii.parent
	left join `tabSales Order Item` as soi on sii.so_detail = soi.name
	left join `tabQuotation Item` as quoi on soi.prevdoc_docname = quoi.parent and soi.item_code = quoi.item_code
	left join `tabQuotation` as quo on quoi.parent = quo.name
	left join `tabReview Query` as revq on quo.review_query = revq.name where {}""".format(conditions))