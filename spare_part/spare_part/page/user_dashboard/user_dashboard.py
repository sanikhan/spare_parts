import frappe
from frappe import _
from frappe.utils import getdate, get_first_day, add_months, add_days, formatdate, today, get_first_day, get_last_day
from erpnext.accounts.utils import get_fiscal_year

@frappe.whitelist()
def get_context():
	fiscal_year = frappe.db.get_default("fiscal_year") or frappe.get_all("Fiscal Year")[0].name
	data = frappe._dict()
	get_charts(data, fiscal_year, "Today")
	
	get_charts(data, fiscal_year, "Yearly")
	get_charts(data, fiscal_year, "Monthly")
	#data['zstatus'] = get_quotation_status()
	print("-----------")
	print(data)
	print("------------")
	return data

def get_charts(data, fiscal_year, type):
	labels = ["Query", "Quotation", "Lost","Closed"]
	quoeries = get_data(fiscal_year, "Query", 'creation', type)
	review_quoeries = get_data_review_query(fiscal_year, "Review Query", 'creation', type)
	quotations_lost = get_data_lost(fiscal_year, "Quotation", 'creation', type)
	sales_order_closed = get_data_closed(fiscal_year, "Sales Invoice", 'creation', type)
	values = [quoeries, review_quoeries,quotations_lost,sales_order_closed]

	if type =="Monthly":
		chart_title = _("This Month")
	elif type =="Yearly":
		chart_title = _("This Year")
	else:
		chart_title = _(type)
	data[type] = {
		"type": "percentage",
		"chart_title": chart_title,
		"colors": ["blue", "yellow","red","green"],
		"data": {
			'labels': labels,
			'datasets': [
				{"name": _(type), "values":values}
			]
		},
		"height": 300,
		"width": 300
	}


def get_data(fiscal_year, doctype, date_field='creation', type="Today"):
	print("Fiscal year",fiscal_year)
	if type=="Today":
		end_date, start_date = [today(), today()]
	elif type=="Monthly":
		end_date, start_date = [get_last_day(today()), get_first_day(today())]
	else:
		end_date, start_date = frappe.db.get_value("Fiscal Year", {"name": fiscal_year}, ['year_end_date', 'year_start_date'])
		print("Start date", start_date)
		print("End date", end_date)
		print("Date field", date_field)
	nom_of_hd = frappe.db.sql("""select count(*) from `tabQuery` where date({p_date})
	 between date('{s_date}') and date('{e_date}') and docstatus=1""".format(p_date=date_field, doc=doctype, s_date=start_date, e_date=end_date))
	return nom_of_hd[0][0]

def get_data_review_query(fiscal_year, doctype, date_field='creation', type="Today"):
	if type=="Today":
		end_date, start_date = [today(), today()]
	elif type=="Monthly":
		end_date, start_date = [get_last_day(today()), get_first_day(today())]
	else:
		end_date, start_date = frappe.db.get_value("Fiscal Year", {"name": fiscal_year}, ['year_end_date', 'year_start_date'])
	nom_of_hd = frappe.db.sql("""select count(*) from `tabQuery` where date({p_date})
	 between date('{s_date}') and date('{e_date}') and status in ('Quotation') and docstatus=1""".format(p_date=date_field, doc=doctype, s_date=start_date, e_date=end_date))
	return nom_of_hd[0][0]

def get_data_lost(fiscal_year, doctype, date_field='creation', type="Today"):
	if type=="Today":
		end_date, start_date = [today(), today()]
	elif type=="Monthly":
		end_date, start_date = [get_last_day(today()), get_first_day(today())]
	else:
		end_date, start_date = frappe.db.get_value("Fiscal Year", {"name": fiscal_year}, ['year_end_date', 'year_start_date'])
	nom_of_hd = frappe.db.sql("""select count(*) from `tab{doc}` where date({p_date})
	 between date('{s_date}') and date('{e_date}') and status in ('Lost') and docstatus=1""".format(p_date=date_field, doc=doctype, s_date=start_date, e_date=end_date))
	return nom_of_hd[0][0]

def get_data_closed(fiscal_year, doctype, date_field='creation', type="Today"):
	if type=="Today":
		end_date, start_date = [today(), today()]
	elif type=="Monthly":
		end_date, start_date = [get_last_day(today()), get_first_day(today())]
	else:
		end_date, start_date = frappe.db.get_value("Fiscal Year", {"name": fiscal_year}, ['year_end_date', 'year_start_date'])
	nom_of_hd = frappe.db.sql("""select count(*) from `tab{doc}` where date({p_date})
	 between date('{s_date}') and date('{e_date}') and sales_order is not null and docstatus=1""".format(p_date=date_field, doc=doctype, s_date=start_date, e_date=end_date))
	return nom_of_hd[0][0]				