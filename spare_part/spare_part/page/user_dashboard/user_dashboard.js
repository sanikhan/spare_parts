frappe.pages['user_dashboard'].on_page_load = function(wrapper) {
	var page = frappe.ui.make_app_page({
		parent: wrapper,
		title: 'Dashboard',
		single_column: true
	});
	$wrapper = $(`<div class="col-xs-12" id="charts"></div>`).appendTo(page.main)
	frappe.call({
		method: "spare_part.spare_part.page.user_dashboard.user_dashboard.get_context",
		callback: r => {
			$wrapper.html("")
			if (!r.xhr) {
				for (let i in r.message || []){
					$(`<div id="chart-${i}" ${i !== __("Monthly")?'class="col-sm-6"':''}>`).appendTo($wrapper);
					new Chart('#chart-'+i, r.message[i])
				}
			}
		}
	})
}