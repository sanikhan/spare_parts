// Copyright (c) 2018, Mainul Islam and contributors
// For license information, please see license.txt
frappe.provide('spare_part.query')

{% include 'spare_part/public/js/engine_options.js' %}
frappe.provide("erpnext.selling");


erpnext.selling.SalesOrderController = erpnext.selling.SalesOrderController.extend($.extend(new spare_part.engine.QueryOptions({frm: cur_frm}), {
    setup: function (){
        this._super()
		this.set_child_filed_options()
		
    },
	customer() {
        this._super()
		this.set_child_filed_options()
    },
//    make_sales_invoice: function() {
		//console.log("dhorse");
//		frappe.call({
//			method: 'frappe.client.get_count',
//			args: {
//				'doctype': 'Sales Invoice',
//				'filters': {'customer_name': cur_frm.doc.customer_name,
//				            'status':'Overdue'
//			              }
//			},
//			callback: function(r) {
//				if (!r.exc) {
//					console.log(r.message)
//					console.log(me.frm)
//					if(r.message==0 || frappe.user.has_role("kebd admin")){
//						//console.log("sssssssss")
//						frappe.model.open_mapped_doc({
//							method: "spare_part.utils.sales.make_invoice_from_so",
//							frm: me.frm
//						})
//					}
//					else{
//						alert("Previous Payment is not Submitted!");
//					}
//
//				}
//			}
//		});
//	},
    make_sales_invoice: function() {    //To stuck customer without payment of last invoice
		console.log("dhorse");
		frappe.call({
			method: 'spare_part.utils.queries.get_invoice_count',
			args: {
                'cus_name': cur_frm.doc.customer_name
			},
			callback: function(r) {
				if (!r.exc) {
					console.log(r.message)
					console.log(me.frm)
					if(r.message==0 || frappe.user.has_role("kebd admin")){
						//console.log("sssssssss")
						frappe.model.open_mapped_doc({
							method: "spare_part.utils.sales.make_invoice_from_so",
							frm: me.frm
						})
					}
					else{
						alert("Previous Payment is not Submitted!");
					}

				}
			}
		});
	},
   make_delivery_note: function() {
		// alert(cur_frm.doc.customer);
		frappe.call({
			method: 'spare_part.utils.queries.get_invoice_count',
			args: {
                'cus_name': cur_frm.doc.customer_name
			},
			callback: function(r) {
				if (!r.exc) {
					console.log(r.message)
					console.log(me.frm)
					if(r.message==0 || frappe.user.has_role("kebd admin")){
						frappe.model.open_mapped_doc({
							method: "erpnext.selling.doctype.sales_order.sales_order.make_delivery_note",
							frm: me.frm
						})
					}
					else{
						alert("Previous Payment is not Submitted!");
					}
					
				}
			}
		});
		
	},
	supplier:function(frm,cdn,cdt){
		
		 var i;
		 console.log(cur_frm.doc.supplier_delivers_to_customer);
		 if(cur_frm.doc.supplier_delivers_to_customer==1 && cur_frm.doc.supplier!=undefined && this.from_markup==undefined){
			 this.from_markup = true
			for (i = 0; i <cur_frm.doc.items.length; i++) {
				cur_frm.doc.items[i].delivered_by_supplier=1;
				cur_frm.doc.items[i].supplier = cur_frm.doc.supplier;
			  }
		 }
		 else if(cur_frm.doc.supplier_delivers_to_customer==0 && cur_frm.doc.supplier==undefined){
			 
			for (i = 0; i <cur_frm.doc.items.length; i++) {
				cur_frm.doc.items[i].delivered_by_supplier=0;
				cur_frm.doc.items[i].supplier = undefined;
			  }
		 }
		 else if(cur_frm.doc.supplier_delivers_to_customer==1 && cur_frm.doc.supplier!=undefined && this.from_markup == true){
			 
	
			 cur_frm.script_manager.trigger(cdn,cdt,"supplier");

		 }

	},

}))

cur_frm.script_manager.make(erpnext.selling.SalesOrderController);


frappe.ui.form.on("Sales Order", {
	onload: function(frm) {
		console.log("custom")
		frm.remove_custom_button("Work Order", 'Create');
        frm.remove_custom_button("Material Request", 'Create');
		
	    //to hide more than one button
        //frm.remove_custom_button(["Installation Note", "Sales Return"], 'Make');
	}
});
