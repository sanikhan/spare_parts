frappe.provide('spare_part.query')

{% include 'spare_part/public/js/engine_options.js' %}
spare_part.query.CommonControlMake = spare_part.engine.QueryOptions.extend({
	setup() {
		this.set_child_filed_options()
		this.frm.add_fetch('item_code', 'item_name', 'item_name')
		this.frm.add_fetch('item_code', 'stock_uom', 'uom')
		this.frm.add_fetch('item_code', 'description', 'description')
		this.frm.add_fetch('item_code', 'item_group', 'item_group')
		this.frm.add_fetch('item_code', 'brand', 'brand')
		this.frm.add_fetch('item_code', 'image', 'image')
	},
	customer() {
		this.set_child_filed_options()
	}
})
spare_part.query.CommonController = frappe.ui.form.Controller.extend(new spare_part.query.CommonControlMake({frm: cur_frm}))
