frappe.ui.form.ControlAttach = frappe.ui.form.ControlAttach.extend({
    set_disp_area: function(value) {
        this._super(value)
        let formated_value = $(this.disp_area).html()
        let html = `<div class="ellipsis">
            <i class="fa fa-paperclip"></i>
            <a class="attached-file-link" target="_blank" href="${formated_value}">${formated_value}</a>
        </div>`;
		this.disp_area && $(this.disp_area).html(html);
	}
})