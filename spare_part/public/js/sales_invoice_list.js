const chart = {
    onload(list) {
        new window.make_status_char_for_list(list)
    }
};

if (frappe.listview_settings['Sales Invoice']) {
    $.extend(frappe.listview_settings['Sales Invoice'], chart)
}
 else {
    frappe.listview_settings['Sales Invoice'] = chart
 }