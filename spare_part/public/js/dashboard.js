const new_one = function () {
	let wrapper = frappe.desktop.wrapper
	$wrapper = $(`<div class="position" style="background-color: white;bottom: 0;padding-bottom: 30px;width: 100%;" class="col-xs-12" id="charts"></div>`).appendTo(wrapper)
	frappe.call({
		method: "spare_part.spare_part.page.user_dashboard.user_dashboard.get_context",
		callback: r => {
			
			$wrapper.html("")
			if (!r.xhr) {
				for (let i in r.message || []) {
					var data = r.message[i].data.datasets[0].values;

					$(`<div class="col-sm-6 col-md-4"> <h3 class="text-center">${r.message[i].chart_title}</h3><div id="chart-${i}"></div>
					<div class="bordered-div">
						<p><b style="margin-left: 40px;">Query:</b> ${data[0]}</p>
					
						<p><b style="margin-left: 40px;">Quotation:</b> ${((data[1]*100)/data[0]).toFixed(2)}%</p>
					
					
						<p><b style="margin-left: 40px;">Lost:</b> ${((data[2]*100)/data[0]).toFixed(2)}%<p>
					
					
						<p><b style="margin-left: 40px;">Closed:</b> ${((data[3]*100)/data[0]).toFixed(2)}%</p>

					</div>	
						
					
					</div>`).appendTo($wrapper);
					new Chart('#chart-' + i, r.message[i])




				}


			}
		}
	})
}

frappe.pages['desktop'].on_page_load = function (old) {
	function rxsist_func(wrapper) {
		old(wrapper)
		setTimeout(() => {
			new_one()
		}, 500)
	}
	return rxsist_func;
}(frappe.pages['desktop'].on_page_load)

$(document).ready(function () {
	$(window).resize(function () {
		let charts = $('.frappe-chart.graphics.graph-focus-margin');
		charts.each(function () {
			data = $(this).children();
			$(this).empty();
			let onlyChilder = [];
			onlyChilder.push(data[0]);
			onlyChilder.push(data[1]);
			$(this).children(onlyChilder);
		});
	});
});