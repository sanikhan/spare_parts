frappe.provide('spare_part.engine')

spare_part.engine.QueryOptions = Class.extend({
	set_child_filed_options() {
		if (!this.frm.doc.customer)
			return;
		let me = this;
		frappe.call({
			method: "frappe.client.get",
			args: {
				doctype: 'Customer',
				name: me.frm.doc.customer
			},
			callback: r => {
				if(!r.xhr) {
					me.child_filed_options = {}
					me.child_filed_sl_options = []
					$.each(r.message.engines, (i, eng)=> {
						if(!me.child_filed_options[eng.engine_model])
							me.child_filed_options[eng.engine_model] = {
								"type": eng.model_type,
								"sl": []
							}
						me.child_filed_options[eng.engine_model].sl.push(eng.engine_sl)
						me.child_filed_sl_options.push(eng.engine_sl)
					})
					frappe.meta.get_docfield("Query Engine", "engine_model", me.frm.doc.name).options = [""].concat(Object.keys(me.child_filed_options));
					frappe.meta.get_docfield("Query Engine", "engine_sl", me.frm.doc.name).options = [""].concat(me.child_filed_sl_options);
				}
			}
		})
	},
	engine_model(doc, cdt, cdn) {
		let row = frappe.get_doc(cdt, cdn)
		if (row.engine_sl && this.child_filed_options[row.engine_model].sl.includes(row.engine_sl)) {
			return
		}else if (row.engine_sl) {
			show_alert(__("Serial must be in '{}'", [this.child_filed_options[row.engine_model].sl.join(',')]))
			row.engine_sl = this.child_filed_options[row.engine_model].sl[0]
		}else {
			row.engine_sl = this.child_filed_options[row.engine_model].sl[0]
		}
		row.model_type = this.child_filed_options[row.engine_model].type
		refresh_field('engines')		
	},
	engine_sl(doc, cdt, cdn){
		let row = frappe.get_doc(cdt, cdn)
		if (!row.engine_model) {
			show_alert(__("Please Select Engine Model first."))
			row.engine_sl = ''
		}
		if (!this.child_filed_options[row.engine_model].sl.includes(row.engine_sl)) {
			show_alert(__("Serial must be in '{}'", [this.child_filed_options[row.engine_model].sl.join(',')]))
			row.engine_sl = this.child_filed_options[row.engine_model].sl[0]
		}
		refresh_field('engines')
	}
})