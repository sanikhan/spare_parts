// Copyright (c) 2018, Mainul Islam and contributors
// For license information, please see license.txt
frappe.provide('spare_part.query')

{% include 'spare_part/public/js/engine_options.js' %}

cur_frm.add_fetch("item_code", "country_of_origin", "country_of_origin")
erpnext.selling.QuotationController = erpnext.selling.QuotationController.extend($.extend(new spare_part.engine.QueryOptions({frm: cur_frm}), {
    setup: function (){
        this._super()
        this.set_child_filed_options()
        this.frm.set_df_property('default_markup_rate_or_amount', 'depends_on', 'eval:doc.default_markup_type')
    },
    refresh: function(doc, dt, dn) {
//        console.log("hello from root")
        this._super(doc, dt, dn)
        if (this.frm.doc.amended_from && this.frm.doc.__islocal) {
            this.frm.set_value("transaction_date", frappe.datetime.get_today())
        }
        if (this.frm.doc.docstatus===0) {
			this.frm.add_custom_button(__('Review Query'),
				function() {
					var setters = {};
					if(me.frm.doc.customer) {
						setters.customer = me.frm.doc.customer || undefined;
                    }
                    me.frm.called_customer_method = false
					erpnext.utils.map_current_doc({
						method: "spare_part.utils.queries.make_quotation",
						source_doctype: "Review Query",
						target: me.frm,
						setters: setters,
						get_query_filters: {
							status: ["not in", ["Lost", "Closed"]]
						}
					})
                }, __("Get items from"), "btn-default");
            this.frm.page.remove_inner_button(__("Opportunity"), __("Get items from"))
        }
        if (this.frm.doc.docstatus === 1) {
			this.frm.add_custom_button(__('Amend'), function() {
                me.frm.amend_doc()
            }).addClass("btn-primary")
        }

        if (this.frm.doc.customer && !this.frm.called_customer_method){
            this.frm.script_manager.trigger('customer')
            this.frm.called_customer_method = true
        }
        if (!this.frm.doc.quotation_body)
            this.frm.set_value('quotation_body', 'Dear Sir, <br> We are submitting the Quotation and the terms and conditions are given below:')
    },
	customer() {
//        console.log("hello from customer")
        this._super()
        this.set_child_filed_options()
	},
    set_dynamic_labels: function () {
//        console.log("hello from set_dynamic_labels")
        let _get_from_super = false;
        if (this.frm.doc_currency !== this.frm.doc.currency
            || this.frm.doc_currency !== this.frm.doc.price_list_currency) {
            _get_from_super = true
        }
        this._super()
        if (_get_from_super) {
            this.set_grid_label_for_markup()
        }
    },
    calculate_item_values: function () {
        //console.log("hello from calculate_item_values")
        let me = this;
        if (!this.discount_amount_applied) {
            $.each(this.frm.doc["items"] || [], function(i, item) {
                item.total_markup_amount = flt(item.markup_amount * item.qty, precision("markup_amount", item));
                if (me.from_markup) {
                    item.rate = item.markup_amount;
                }else if (item.markup_type) {
                    me.set_markup_amount(item)
                }

				me.set_in_company_currency(item, ["markup_amount", "total_markup_amount"]);
			});
        }
        this._super()
    },
    _quotation_for: function() {
       // console.log("hello from _quotation_for")
        this.calculate_taxes_and_totals()
        this.set_default_values()
    },
    set_default_values: function() {
       // console.log("hello from set_default_values")
        let quotation_for = this.frm.doc._quotation_for
        let me = this;
        let values = {
            Local: {
                currency: "BDT",
                tc_name: "Local Quotation Terms",
                letter_head: "Local Letterhead",
                default_markup_base_on: "Incoming Rate"
            },
            Foreign: {
                currency: "EUR",
                tc_name: "Foreign Quotation",
                letter_head: "Foreign Letterhead",
                default_markup_base_on: "Price List Rate"
            }
        }
        $.each(values[quotation_for], (k, v) => {
            me.frm.set_value(k, v)
        })
    },
    manual_calculate: function() {
//        console.log("hello from manual_calculate")
        this.calculate_taxes_and_totals()
    },
    calculate_taxes_and_totals: function(update_paid_amount) {
        //console.log("hello from calculate_taxes_and_totals")
        this._super(update_paid_amount)
        this.total_cost_and_others()
        if (this.frm.doc._quotation_for=="Foreign") {
            this.set_totals_and_profit()
        }
        this.set_profit()
    },
    total_cost_and_others: function() {
        //console.log("hello from total_cost_and_others")
        if (this.frm.doc._quotation_for=="Local") {
            this.frm.set_value('total_cost', this.get_cost())
        }else if (this.frm.doc._quotation_for=="Foreign") {
            this.frm.set_value('material_cost', this.get_cost())
            this.frm.set_value('special_discount_amount', flt(this.frm.doc.material_cost)/100 * (flt(this.frm.doc.special_discount_percent) || 0))
            this.frm.set_value('material_cost_after_discount', (this.frm.doc.material_cost || 0) - (this.frm.doc.special_discount_amount || 0))
            if (this.frm.doc.packing_and_handling_cost_percent) {
                this.frm.set_value('packing_and_handling_cost_amount', ((flt(this.frm.doc.material_cost_after_discount) || 0)/100)*flt(this.frm.doc.packing_and_handling_cost_percent))
            }
            let total_cost = flt(this.frm.doc.material_cost_after_discount || 0) +
                            flt(this.frm.doc.packing_and_handling_cost_amount || 0) +
                            flt(this.frm.doc.lc_cost || 0) + flt(this.frm.doc.air_freight_charge || 0)
            this.frm.set_value('total_cost', total_cost)
        }
    },
    set_totals_and_profit: function() {
//        console.log("hello from set_totals_and_profit")
        this.frm.set_value('sales_after_discount', this.frm.doc.net_total)
        if (this.frm.doc.sales_packing_and_handling_cost_percent) {
            this.frm.set_value('sales_packing_and_handling_cost_amount', flt(this.frm.doc.sales_after_discount)/100 * flt(this.frm.doc.sales_packing_and_handling_cost_percent))
        }
        else {
            this.frm.set_value('sales_packing_and_handling_cost_amount', 0)
        }
        this.frm.set_value('total_sales', this.frm.doc.sales_after_discount +
         (this.frm.doc.sales_packing_and_handling_cost_amount || 0) +
         (this.frm.doc.sales_air_freight_charge || 0));

        if (this.frm.doc.tac_percent || this.frm.doc.tac) 
            this.frm.set_value('tac', flt(this.frm.doc.total_sales)/100 * flt(this.frm.doc.tac_percent || 0));
        this.frm.set_value('sales_after_tac', this.frm.doc.total_sales - (this.frm.doc.tac || 0))
    },
    sales_packing_and_handling_cost_percent: function(){
//        console.log("hello from sales_packing_and_handling_cost_percent")
        if (!this.frm.doc.sales_packing_and_handling_cost_percent || this.frm.doc.sales_packing_and_handling_cost_percent===0) {
            this.frm.set_value('sales_packing_and_handling_cost_amount', 0)
        }
    },
    set_profit:function() {
//        console.log("hello from set_profit")
        let profit;
        if(this.frm.doc._quotation_for=="Foreign") {
            this.frm.set_value('net_profit_amount', flt(this.frm.doc.sales_after_tac) - flt(this.frm.doc.total_cost))
            profit = flt(flt(this.frm.doc.net_profit_amount)/flt(this.frm.doc.total_sales));
        }else {
            this.frm.set_value('net_profit_amount', flt(this.frm.doc.grand_total)- flt(this.frm.doc.total_taxes_and_charges || 0) - flt(this.frm.doc.total_cost))
            profit = flt(flt(this.frm.doc.net_profit_amount || 0)/(flt(this.frm.doc.grand_total || 0) - flt(this.frm.doc.total_taxes_and_charges || 0)));
        }
        this.frm.set_value('net_profit', roundNumber(flt(profit)*100, precision('net_profit', this.frm.doc)
        ))
    },
    get_cost: function(without_discount) {
//        console.log("hello from get_cost")
        let me = this;
        let totals = 0.0;
        me.frm.doc.items.forEach(row => {
            totals += flt(me.get_cost_rate(row, without_discount) * flt(row.qty))
        });
        return totals
    },
    set_grid_label_for_markup: function () {
//        console.log("hello from set_grid_label_for_markup")
        var company_currency = this.get_company_currency();
        this.frm.set_currency_labels(["base_markup_amount", "base_total_markup_amount"],
            company_currency, "items");

        this.frm.set_currency_labels(["markup_amount", "total_markup_amount"],
            this.frm.doc.currency, "items");

        // toggle columns
        var item_grid = this.frm.fields_dict["items"].grid;
        $.each(["base_markup_amount", "base_total_markup_amount"], function (i, fname) {
            if (frappe.meta.get_docfield(item_grid.doctype, fname))
                item_grid.set_column_disp(fname, me.frm.doc.currency != company_currency);
        });
    },
    markup_type: function (doc, cdt, cdn) {
//        console.log("hello from markup_type")
        let row = frappe.get_doc(cdt, cdn)
        if (row.markup_type)
            this.markup_rate_or_amount(doc, cdt, cdn);
    },
    markup_rate_or_amount: function (doc, cdt, cdn) {
//        console.log("hello from markup_rate_or_amount")
        let row = frappe.get_doc(cdt, cdn)
        this.set_markup_amount(row)
        this.from_markup = true
        this.calculate_item_values()
        refresh_field('items')
    },
    set_markup_amount: function(row) {
//        console.log("hello from set_markup_amount")
        let val_rate = this.get_cost_rate(row)
//        console.log("valuation_rate in set_markup_amount fun:"+val_rate);
        if (row.markup_type === "Percentage") {
            row.markup_amount = val_rate * (row.markup_rate_or_amount / 100)
        } else if (row.markup_type === "Amount") {
            row.markup_amount = val_rate + row.markup_rate_or_amount
        } else if (row.markup_type === "Manually Amount") {
            row.markup_amount = row.markup_rate_or_amount
        }
    },
    get_cost_rate: function(row, without_discount) {
//        console.log("hello from get_cost_rate")
//        console.log(row)
        if (!row)
            return 0;
        let me = this;
        let rate_list = {
            "Incoming Rate": 'incoming_rate',
            "Price List Rate": 'price_list_rate'
        }
        let valuation_rate = rate_list[row['markup_base_on']];
        console.log("get cost rate");

            if(row.valuation_rate)
            {
                frappe.call(                                // this call has added to fetch incoming rate from stock entry 
                {
                    method: 'frappe.client.get_value',
                args: {
                    'doctype': 'Stock Ledger Entry',
                    'filters': {'batch_no': row.batch_no},
                    'fieldname': [
                        'incoming_rate'
                    ]
                    },async:false,
                callback: function(r) {
                    if (!r.exc) {
                        row[valuation_rate] = r.message.incoming_rate; // putting incoming rate in valuation frate variable
                        row.incoming_rate = row[valuation_rate]     // set incoming rate to incoming rate field
                        console.log(row.incoming_rate);
                        frappe.model.set_value("incoming_rate",r.message.incoming_rate);

                        }
                    }
                }); 
            }
        
        let val_rate = flt(row[valuation_rate]);
        if (me.frm.doc.currency != me.get_company_currency())
            val_rate = flt(row[valuation_rate]) / flt(me.frm.doc.conversion_rate)
        if (without_discount && me.frm.doc.special_discount_percent)
            val_rate = (val_rate/100)*(100-flt(me.frm.doc.special_discount_percent))
        return val_rate
    },
    batch_no: function(doc, cdt,cdn) 
    {
//        console.log("hello from batch_no")
       // this.frm.set_value("valuation_rate","00");
        this._super(doc, cdt,cdn)
        let row = frappe.get_doc(cdt, cdn)
        // var incoming_rate1 = 0.00;
        
        if (row.batch_no) 
        {
            this.item_code(doc,cdt,cdn)
           
            
        }         
    },
    item_code: function (doc, cdt, cdn) {
//        console.log("hello from item_code")
        let me = this;
        let row = frappe.get_doc(cdt, cdn)
      //  console.log(row)        // show if icoming_rate is added on item_code 
        let batch_no = row.batch_no
        this._super(doc, cdt, cdn).then(function() {
            if (!row.reference_price)
                me.set_ref_price(row, doc.customer);
            row.batch_no = batch_no
            if (row.markup_rate_or_amount)
                me.markup_rate_or_amount(doc, cdt,cdn)              
        })
    },
    set_ref_price: function(row, customer) {
//        console.log("hello from set_ref_price")
        if (!row.item_code || !customer)
            return;
        frappe.call({
            method:"spare_part.utils.queries.get_reference_rate",
            args: {
                'item_code': row.item_code,
                'customer': customer
            },
            callback: r => {
                if(r['message'] && r.message.length)
                    row.reference_price = r.message[0][0]
            }
        })
    },
    items_add: function(doc, cdt,cdn) {
//        console.log("hello from items_add")
        let row = frappe.get_doc(cdt, cdn)
        if (doc.default_markup_base_on)
            row.markup_base_on = doc.default_markup_base_on;
        if (doc.default_markup_type)
            row.markup_type = doc.default_markup_type;
        if (doc.default_markup_rate_or_amount)
            row.markup_rate_or_amount = doc.default_markup_rate_or_amount;
        
    },
    default_markup_rate_or_amount: function(doc) {
//        console.log("hello from default_markup_rate_or_amount")
         if (doc.default_markup_rate_or_amount && doc.items){
             doc.items.forEach(row=>{
                 if (!row.markup_type || !row.markup_rate_or_amount || (row.markup_type === doc.default_markup_type) && row.markup_base_on === doc.default_markup_base_on){
                     row.markup_base_on = doc.default_markup_base_on;
                     row.markup_type = doc.default_markup_type;
                     row.markup_rate_or_amount = doc.default_markup_rate_or_amount;
                     this.markup_rate_or_amount(doc, row.doctype, row.name)
                 }
             })
             this.calculate_taxes_and_totals()
         }
        this.overwrite_child_table_markup_value(doc)
    },
    default_markup_base_on: function(doc) {
//        console.log("hello from default_markup_base_on")
        this.overwrite_child_table_markup_value(doc)
    },
    default_markup_type: function(doc) {
//        console.log("hello from default_markup_type")
        this.overwrite_child_table_markup_value(doc)
    },
    overwrite_child_table_markup_value: function(doc) {
//       console.log("hello from overwrite_child_table_markup_value")
        if (doc.items){
            doc.items.forEach(row=>{
                row.markup_base_on = doc.default_markup_base_on;
                row.markup_type = doc.default_markup_type;
                row.markup_rate_or_amount = doc.default_markup_rate_or_amount;
                this.markup_rate_or_amount(doc, row.doctype, row.name)
                this.from_markup = false
            })
            this.calculate_taxes_and_totals()
        }
    },
    rate:function(doc){
//        console.log(cur_frm.doc.doctype)
//        console.log("hello from rate")
        this.from_markup = false
    },
}))

cur_frm.script_manager.make(erpnext.selling.QuotationController);
