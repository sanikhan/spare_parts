const chart = {
    onload(list) {
        new window.make_status_char_for_list(list)
    }
};

if (frappe.listview_settings['Quotation']) {
    $.extend(frappe.listview_settings['Quotation'], chart)
}
 else {
    frappe.listview_settings['Quotation'] = chart
 }