$(document).on('toolbar_setup', function (event) {
    let $header = $('.navbar-fixed-top');
    $header.find('.navbar-right > .dropdown-help, .navbar-right > .frappe-chat-dropdown').remove()
    $header.find('.navbar-brand').html(`<img class="erpnext-icon" src="/assets/spare_part/images/favicon.ico">`)
})

$(document).on('page-change', function() {
    let routes = frappe.get_route()
    if (routes && routes[0]==="List") {
        let get_sidebar = false;
        let set_interval = setInterval(function(){
            if(typeof cur_list.list_sidebar.sidebar !== "undefined") {
                get_sidebar = true
                let $sidebar = $(cur_list.list_sidebar.sidebar)
                $sidebar.find('[data-view="Kanban"], [data-view="Calendar"], [data-view="Gantt"], [data-view="Image"]').hide()
                $sidebar.find('.help-link').parent().hide()
            }
            if (get_sidebar) 
                clearInterval(set_interval);
        })
    }else if(routes && routes[0]=="modules") {
        $(cur_page.page).find('.layout-side-section').hide()
        $(cur_page.page).find('.layout-main-section-wrapper').addClass('col-md-12').removeClass('col-md-10')
    }
})


window.make_status_char_for_list = function(list) {
    if (typeof list === "undefined")
        return;
    if (!$(`chart${frappe.scrub(list.doctype)}`).length){
        $(list.page.inner_toolbar).removeClass('hide')
        $(`<div class="row"><div class="col-xs-12" id="chart${frappe.scrub(list.doctype)}"></div></div>`).prependTo(list.page.inner_toolbar)
    }
    frappe.call({
        method: "spare_part.utils.queries.get_status",
        args:{
            doctype: list.doctype
        },
        freeze: true,
        callback: r => {
            if (r['message'])
                list.status_chart = new Chart(`#chart${frappe.scrub(list.doctype)}`, r.message);
        }
    })
}