// Copyright (c) 2018, Mainul Islam and contributors
// For license information, please see license.txt
frappe.provide('spare_part.query')

{% include 'spare_part/public/js/engine_options.js' %}
frappe.provide("erpnext.accounts");


erpnext.accounts.SalesInvoiceController = erpnext.accounts.SalesInvoiceController.extend($.extend(new spare_part.engine.QueryOptions({frm: cur_frm}), {
    setup: function (){
        this._super()
		this.set_child_filed_options()
    },
	customer() {
        this._super()
		this.set_child_filed_options()
    }
    
}))

cur_frm.script_manager.make(erpnext.accounts.SalesInvoiceController);


cur_frm.cscript['Make Delivery Note'] = function() {
    console.log(cur_frm.doc.status);
    if(cur_frm.doc.status!="Overdue" || frappe.user.has_role("kebd admin")){
    frappe.model.open_mapped_doc({
        method: "spare_part.utils.sales.make_delivery_note",
        frm: cur_frm
    })
}
   else{
       alert("Previous Payment is not Submitted!");
   }
}
// if(doc.docstatus == 1 && doc.outstanding_amount != 0
//     && !(doc.is_return && doc.return_against)) {
//     this.frm.add_custom_button(__('Payment'), this.make_payment_entry, __('Create'));
//     cur_frm.page.set_inner_btn_group_as_primary(__('Create'));
// }

// frappe.ui.form.on('Sales Invoice', {
//     on_submit: function(frm) {
//         // alert("yappp");
//         return frappe.call({
// 			method: "spare_part.utils.sales.get_payment_entry",
// 			args: {
// 				"dt": frm.doc.doctype,
// 				"dn": frm.doc.name
// 			},
// 			callback: function(r) {
// 				var doc = frappe.model.sync(r.message);
//                 // frappe.set_route("Form", doc[0].doctype, doc[0].name);

// 			}
// 		});
//     }
// })


